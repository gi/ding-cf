# ding-cf

The capping-free version of [dingII](https://gitlab.ub.uni-bielefeld.de/gi/dingiiofficial).

**Warning: Before using the drosophila workflow (subdirectory `workflow`), read the section on the workflow below!**


A typical way of using ding-cf as a standalone under the maximal matching model would look like this:
1.  Generate the ILP: `./ding_cf.py  {unimog-file} -mm --writeilp {ilp-file}`
2.  Use a solver to obtain a gurobi solution `{gurobi-sol}`.
3.  Get the matching and distance: `./dingII_parsesol.py {unimog-file} --solgur {gurobi-sol} --matching {unimog-matching} `


An overview of the scripts dealing directly with the DING-CF ILP:


|script  | purpose | input | output | dependencies |
| ------ | ------ | ------ | ------ | ------ |
|  ding\_cf.py | Generate the DING-CF ILP or create a custom model file in order to fine tune how many genes per family are to be matched | [UniMoG file](https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_manual) containing a single genome pair, (custom model file)  | [gurobi lp file](https://www.gurobi.com/documentation/9.1/refman/lp_format.html) or custom model file | python3, networkx, dingII\_util, ilp\_util\_adj |
| dingcf\_parsesol.py | Calculate the distance and matching | UniMoG file with the original, unmatched genomes, [gurobi solution file](https://www.gurobi.com/documentation/9.1/refman/sol_format.html) containing a single solution with objective value | UniMoG file with relabeled genes according to matching | python3, networkx, dingII\_util, ilp\_util\_adj|
| dingII\_util.py | Provide utility functions for MRD generation and solution parsing | - | - | python3,networkx, ilp\_util\_adj |
ilp\_util\_adj.py | Provide further utility functions | - | - | python3 |


### Additional arguments
| Argument | Value type/range/number | Explanation |
| -------- | -------- | -------- |
|`-em` | - | Use exemplary matching (exactly 1 marker per family is matched) instead of maximal matching. |
|`-im` | - | Use intermediate matching (at least 1 marker per family is matched) instead of maximal matching. |
| `-r`/`--range` | two floats in `[0,1]` | Like intermediate matching, but uses a percentile of the family size as lower and upper bound for the numbers of markers to be matched |
| `--disable-optimize-comps`| - | Setting this disables the optimization Constraint C.19  |
| `--disable-binary-mode` | - | (Legacy!) Don't use this unless you really like slower and worse algorithms. |

## The Drosophila Workflow Subdirectory

The `workflow` subdirectory contains the workflow for the drosophila dataset, but **not** the data itself, so you will need to configure some things before using it.

1. Get the drosophila data here: https://uni-bielefeld.sciebo.de/s/06wGjSlvk7jeuWR
2. Unpack the archive and place `Results_Apr27` and `genbank` in the `workflow` directory.
3. For the `circos` images, there is some more configuration needed:
    1. Install `circos`.
    2. Not all circos images can be generated with the standard maximum number of ideograms, so increase as needed.

### Snakemake rules
| Rule | effect |
| ------ | ------ |
| `all_dist`| Compute the distances of all pairs of taxa specified in `config.yaml`       |
| `dist_phylip`       | Like `all_dist` but generates the file `distances.dist`, a phylip distance file to be used with phylogenetics tools. |
| `dist_tex`       | Like `all_dist` but generates the file `distances.tex`, a tex tabular with distances. Needs the glossary entries below. |
| `all_genome_stat` | Compute the genome statistics as seen in Table E.1. Needs the glossary entries below. |
| `pair_stat` | Compute all-vs-all statistics as seen in Table E.3. Requires `dingII` scripts in script directory and glossary entries below. |
| `circos_tex` | Create subdirectory `img_circos` with all circos plots for all taxa and a tex file which when compiled creates a pdf with the images. |

#### Glossary
To compile the tex tables you need the `glossaries` package (`\usepackage{glossaries}`) and the following definitions:

<code>
\newglossaryentry{dsec}{name=\textit{D.~sechellia},description={}}
\newglossaryentry{dsim}{name=\textit{D.~simulans},description={}}
\newglossaryentry{dmel}{name=\textit{D.~melanogaster},description={}}
\newglossaryentry{dyak}{name=\textit{D.~yakuba},description={}}
\newglossaryentry{dere}{name=\textit{D.~erecta},description={}}
\newglossaryentry{dana}{name=\textit{D.~ananassae},description={}}
\newglossaryentry{dper}{name=\textit{D.~persimilis},description={}}
\newglossaryentry{dgri}{name=\textit{D.~grimshawi},description={}}
\newglossaryentry{dmoj}{name=\textit{D.~mojavensis},description={}}
\newglossaryentry{dvir}{name=\textit{D.~virilis},description={}}
\newglossaryentry{dwil}{name=\textit{D.~willistoni},description={}}
\newglossaryentry{sec}{name=\textit{D.~sec},description={}}
\newglossaryentry{sim}{name=\textit{D.~sim},description={}}
\newglossaryentry{mel}{name=\textit{D.~mel},description={}}
\newglossaryentry{yak}{name=\textit{D.~yak},description={}}
\newglossaryentry{ere}{name=\textit{D.~ere},description={}}
\newglossaryentry{ana}{name=\textit{D.~ana},description={}}
\newglossaryentry{per}{name=\textit{D.~per},description={}}
\newglossaryentry{gri}{name=\textit{D.~gri},description={}}
\newglossaryentry{moj}{name=\textit{D.~moj},description={}}
\newglossaryentry{vir}{name=\textit{D.~vir},description={}}
\newglossaryentry{wil}{name=\textit{D.~wil},description={}}
</code>

