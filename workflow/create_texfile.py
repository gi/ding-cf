PREAMBLE = '''
\\documentclass{article}
\\usepackage[utf8]{inputenc}
\\usepackage{graphicx}
\\begin{document}
'''

POSTAMBLE = '''

\\end{document}

'''



FIGURE = '''
    \\includegraphics[width=0.45\\textwidth]{%s}
    \\includegraphics[width=0.45\\textwidth]{%s}

\\textbf{%s}

'''

from argparse import ArgumentParser

def raw_name(s):
    return s.split('/')[-1].split('-')[0].strip()

def clean(s):
    return s.replace('_','-')

def names(files):
    ret = {}
    for f in files:
        ret[raw_name(f)]=f
    return ret

parser=ArgumentParser()
parser.add_argument('--before',nargs='+')
parser.add_argument('--after',nargs='+')
args = parser.parse_args()

before = names(args.before)
after = names(args.after)
print(PREAMBLE)
for k in sorted(list(set(before.keys()).intersection(after.keys()))):
    print(FIGURE%(before[k],after[k],"%s."%clean(k)))
print(POSTAMBLE)
