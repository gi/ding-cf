from argparse import ArgumentParser
from Bio import SeqIO
import sys

CHR_LINEAR = '|'
CHR_CIRCULAR=')'
ORIENT_NEGATIVE = '-'
ORIENT_POSITIVE = '+'


def to_direction(k):
    if k < 0:
        return ORIENT_NEGATIVE
    return ORIENT_POSITIVE

def parse_genbank_to_chromosomes(gb_file, skip_circulars = False, accepted_genes=[]):
    '''
    Read a genbank file containing the records of chromosomes and parse
    it to a list of unimog-chromosomes consisting of CDS as markers identified by their refseq ids.
    '''
    chromosomes = []
    all_genes = []
    for gb_record in SeqIO.parse(open(gb_file,"r"), "genbank") :
        #records are assumed to be all chromosomes
        #print ("Name %s, %i features" % (gb_record.annotations['topology'], len(gb_record.features)))
        top = gb_record.annotations['topology']
        #list of (start, strand, refseqid)
        cds = []
        if top == 'linear':
            chr_type = CHR_LINEAR
        elif top=='circular':
            chr_type = CHR_CIRCULAR
            if skip_circulars:
                continue
        else:
            print('Unknown chromosome type "%s" on chromosome "%s". This chromosome will be skipped'%(top, gb_record.name),file=sys.stderr)
            continue
        #seen_genes = set()
        for feature in gb_record.features:
            if feature.type=='CDS':
                prid = feature.qualifiers['protein_id']
                if len(prid) > 1:
                    print('Skipping ambiguous id feature: '+prid,file=sys.stderr)
                    continue
                else:
                    prid = prid[0]
                if not prid in accepted_genes:
                    print('Skipping entry: %s ; was not regarded for orthology assignment.'%prid,file=sys.stderr)
                    continue
                #gene = feature.qualifiers['gene']
                #if len(gene) > 1:
                #    print('Skipping ambiguous gene: '+gene)
                #    continue
                #else:
                #    gene = gene[0]
                #if gene in seen_genes:
                #    continue
                loc = feature.location
                if loc.strand == None:
                    print('Skipping ambiguous strand feature:',file=sys.stderr)
                    print(feature,file=sys.stderr)
                    continue
                #seen_genes.add(gene)
                cds.append((loc.start,loc.strand, prid))
                all_genes.append(prid)
                
        cds.sort()
        cds = [(to_direction(strand), prid) for _, strand, prid in cds]
        chromosomes.append((chr_type, cds))
    return chromosomes, all_genes

def id_from_header(s):
    return s.split('|')[1]

def parse_groupfile(f):
    groupmap = {}
    group_sizes = {}
    with open(f) as fl:
        for line in fl:
            groupentry = line.split(': ')
            idd = groupentry[0].strip()
            members = [id_from_header(header) for header in groupentry[1].strip().split()]
            sz = len(members)
            if not sz in group_sizes:
                group_sizes[sz] = 0
            group_sizes[sz]+=1
            for m in members:
                groupmap[m] = idd
    return groupmap,group_sizes

def assign_genes(chroms,fmap):
    return [(orient,[(o,fmap[g]) for (o,g) in ms]) for (orient,ms) in chroms]
    
def print_chroms(chroms):
    for orient, ms in chroms:
        for o, m in ms:
            print('%s%s'%(o,m),end=' ')
        if not len(ms) == 0:
            print(orient)
        else:
            print("Disregarding empty chromosome.",file=sys.stderr)

def main():
    parser = ArgumentParser()
    parser.add_argument('gbfile')
    parser.add_argument('groupfile')
    args = parser.parse_args()
    groupmap,group_sizes = parse_groupfile(args.groupfile)
    chroms, genes = parse_genbank_to_chromosomes(args.gbfile,accepted_genes=list(groupmap.keys()))
    chroms = assign_genes(chroms,groupmap)
    print(sorted(list(group_sizes.items())),file=sys.stderr)
    print('>G')
    print_chroms(chroms)

main()
