from argparse import ArgumentParser
import matplotlib.pyplot as plt
import os

CHR_CIRCULAR = ')'
CHR_LINEAR = '|'
ORIENT_POSITIVE = '+'
ORIENT_NEGATIVE = '-'

HOUSEKEEPING='etc/housekeeping.conf'


CONF_TEXT = '''
# circos.conf

karyotype = {karyoa}, {karyob}


chromosomes_order= {chromsb}
chromosomes_reverse={chromsb}

<ideogram>

<spacing>
default = 0.005r
</spacing>

radius    = 0.9r
thickness = 20p
fill      = yes


</ideogram>



<links>
<link>
file=data/forward.txt
radius=0.95r
bezier_radius=0.1r
thickness=1
color=blue
</link>
<link>
file=data/backward.txt
radius=0.95r
bezier_radius=0.1r
thickness=1
color=purple
</link>

</links>


################################################################
# The remaining content is standard and required. It is imported 
# from default files in the Circos distribution.
#
# These should be present in every Circos configuration file and
# overridden as required. To see the content of these files, 
# look in etc/ in the Circos distribution.

<image>
# Included from Circos distribution.
<<include etc/image.conf>>
</image>

# RGB/HSV color definitions, color lists, location of fonts, fill patterns.
# Included from Circos distribution.
<<include etc/colors_fonts_patterns.conf>>

# Debugging, I/O an dother system parameters
# Included from Circos distribution.
<<include %s>>

'''%HOUSEKEEPING

def readGenomes(data, genomesOnly=None):
    """Read genome in UniMoG format
    (https://bibiserv.cebitec.uni-bielefeld.de/dcj?id=dcj_manual)"""

    res = list()

    # helper function for parsing each individual gene
    str2gene = lambda x: x.startswith(ORIENT_NEGATIVE) and (ORIENT_NEGATIVE, \
            x[1:]) or (ORIENT_POSITIVE, x.lstrip(ORIENT_POSITIVE))
    # process each line, assuming that the file is well-formatted
    skip = False
    for line in data:
        line = line.strip()
        if line:
            if line.startswith('>'):
                genomeName = line[1:].strip()
                if genomesOnly == None or genomeName in genomesOnly:
                    skip = False
                    res.append((genomeName, list()))
                elif genomesOnly:
                    skip = True
            elif line[-1] not in (CHR_CIRCULAR, CHR_LINEAR):
                print(line)
                raise Exception('Invalid format, expected chromosome to ' + \
                        'end with either \'%s\' or \'%s\'' %(CHR_CIRCULAR, \
                        CHR_LINEAR))
            elif not skip:
                res[-1][1].append((line[-1], list(map(str2gene, line[:-1].split()))))
    return res


class IDGenerator:
        def __init__(self):
            self.count = 0
        def get_new(self):
            self.count+=1
            return self.count
            
def add_genes(gnm,fams,gen,coords,gnmname,seen_chroms=[]):
    for i,chrmrrr in enumerate(gnm):
        _,chrm = chrmrrr
        chrmname = gnmname+str(i)
        for j, og in enumerate(chrm):
            orient,gene = og
            if not gene in fams:
                fams[gene] = []
            tid = gen.get_new()
            fams[gene].append((orient,tid))
            coords[tid] = (chrmname,j)
        seen_chroms.append((chrmname,len(chrm)))

COLOR_MAP = {'A0':'red','A1':'red','B0':'blue','B1':'blue'}
        

parser=ArgumentParser()
parser.add_argument("infile")
parser.add_argument("outdir")

#parser.add_argument('--links',nargs=2,default=['forward.txt','backward.txt'])
#parser.add_argument('--karyotypes',nargs=2,default=['A.txt','B.txt'])
#parser.add_argument('--conf',default='circos.conf')

args = parser.parse_args()
print(args.outdir)

links = [os.path.join(args.outdir,'data','%s.txt'%a) for a in ['forward','backward']]
karyotypes = [os.path.join(args.outdir,'data','%s.txt'%a) for a in ['A','B']]
conf = os.path.join(args.outdir,'circos.conf')


with open(args.infile) as f:
    gnms = readGenomes(f.readlines())

famsa = {}
famsb = {}
coords = {}
seen_chroms = []
gen = IDGenerator()
data_dir = os.path.join(args.outdir,'data')

nm1,gnm1 = gnms[0]
nm2,gnm2 = gnms[1]
add_genes(gnm1,famsa,gen,coords,nm1,seen_chroms=seen_chroms)
add_genes(gnm2,famsb,gen,coords,nm2,seen_chroms=seen_chroms)

if not os.path.exists(args.outdir):
    os.makedirs(args.outdir)
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
    
with open(karyotypes[0],'w') as f:
    with open(karyotypes[1],'w') as g:
        i=0
        for nm, l in seen_chroms:
            i+=1
            print("chr - {id} {label} {start} {end} {color}".format(id=nm,label=i,start=0,end=l,color='blue'if nm.startswith('A') else 'red'),file=f if nm.startswith('A') else g)

k1 = set(famsa.keys())
k2 = set(famsb.keys())
with open(links[0],'w') as f:
    with open(links[1],'w') as g:
        for k in k1.intersection(k2):
            for o1,i in famsa[k]:
                chrmi, posi = coords[i]
                for o2,j in famsb[k]:
                    chrmj, posj = coords[j]
                    print("{chrmi} {posi} {posi} {chrmj} {posj} {posj}".format(chrmi=chrmi,posi=posi,chrmj=chrmj,posj=posj),file=f if o1==o2 else g)

    
              
with open(conf,"w") as f:
    print(CONF_TEXT.format(karyoa='data/A.txt',karyob='data/B.txt',chromsb=','.join(reversed([c for c,_ in seen_chroms if c.startswith('B')]))),file=f)
    

    



