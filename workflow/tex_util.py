

FLIES =  ['ananassae','erecta','grimshawi','melanogaster','mojavensis','persimilis','sechellia','simulans','virilis','yakuba','willistoni']
LATEX = {}
LATEX_SHORT = {}

for f in FLIES:
    LATEX[f]= "\\gls{d"+f[0:3]+"}"
    
for f in FLIES:
    LATEX_SHORT[f]= "\\gls{"+f[0:3]+"}"


def name_to_latex(n):
    return "-".join([LATEX[s] for s in n.split("_")])

def name_to_latex_short(n):
    return "-".join([LATEX_SHORT[s] for s in n.split("_")])
    
def raw_name(nm):
    return nm.split('/')[-1].strip().split('.')[0].strip()

def parse_file_name(nm):
    realnm = raw_name(nm)
    taxa = realnm.split('_')
    return taxa
