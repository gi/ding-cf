from argparse import ArgumentParser
import sys
from tex_util import *


def parse_dist(lines, add_gap=False):
    dist = lines[0].split(",")[0].strip()
    if not add_gap:
        return dist
    else:
        gap = float(lines[0].split(",")[1].replace("%","").strip())
        if gap <= 0:
            return "*"+dist
        return dist




selfcomps ={'dist':"0"}


def add_file(fl,mat,add_gap=False):
    t1t2 = parse_file_name(fl)
    t1,t2 = t1t2[0],t1t2[1]
    with open(fl) as f:
        lines = f.readlines()
        entry = parse_dist(lines,add_gap=add_gap)
        if not t1 in mat:
            mat[t1]={}
        mat[t1][t2]=entry

def all_keys(mat):
    keyz = set(mat.keys())
    for _,m2 in mat.items():
        for k in m2:
            keyz.add(k)
    return sorted(list(keyz))
    

def make_symmetric(mat,selfcomp='0'):
    keyz = all_keys(mat)
    print(mat,file=sys.stderr)
    for k in keyz:
        if not k in mat:
            mat[k]={}
        for l in keyz:
            if k==l:
                mat[k][l]=selfcomp
                continue
            if not l in mat[k]:
                mat[k][l] = mat[l][k]
    return mat

SPACER = " "*4

def phylip_name(s):
    l = len(s)
    if l <= 10:
        return s+((10-l)*" ")
    else:
        return s[0:10]

def output_phylip(mat):
    keyz = all_keys(mat)
    print(SPACER,len(keyz))
    for t1 in keyz:
        print(phylip_name(t1),end=" ")
        print(" ".join([mat[t1][t2] for t2 in keyz]))

def output_latex(mat):
    keyz = all_keys(mat)
    l = len(keyz)
    print("\\begin{tabular}{c|"+(l*'r')+"}")
    print("& ",end="")
    print(" & ".join([name_to_latex_short(t1) for t1 in keyz]),end="\\\\\n")
    print("\\hline")
    for i,t1 in enumerate(keyz):
        print(name_to_latex(t1),end=" ")
        for j,t2 in enumerate(keyz):
            if j < i:
                #skip entries below diagonal
                print(" & ",end="")
                continue
            print(" & %s"%mat[t1][t2],end="")
        print("\\\\")
    print("\\end{tabular}")
    

parser = ArgumentParser()
parser.add_argument('--outfmt',choices=["phylip","tex"],default="phylip")
parser.add_argument('--files',nargs='+',required=True)


args = parser.parse_args()

mat = {}
for f in args.files:
    add_file(f,mat,add_gap=(args.outfmt=="tex"))
    
make_symmetric(mat)
if args.outfmt=="phylip":
    output_phylip(mat)
else:
    output_latex(mat)
