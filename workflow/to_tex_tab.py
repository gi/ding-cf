from tex_util import *
from argparse import ArgumentParser

MAX_FILES = 3
parser = ArgumentParser()
parser.add_argument('-d',default=',')
parser.add_argument('--escape',nargs='*',default=[])
parser.add_argument('--header',nargs='+')
parser.add_argument('--highlight-min',nargs=2,type=int)


for i in range(1,MAX_FILES+1):
    if i==1:
        parser.add_argument("--files",nargs='+',required=True)
    else:
        parser.add_argument("--files%i"%i,nargs='+',default=[])
    parser.add_argument("--f%iexclude"%i,nargs='+',type=int,default=[])

args = parser.parse_args()


def clean(s,escape=[]):
    s_ = s
    for e in escape:
        s_=s_.replace(e,"\\"+e)
    return s_
        

def to_list(line,delim=',',escape=[]):
    return [clean(e.strip(),escape=escape) for e in line.split(delim)]

def add_to_tab(fls,tab,exclude=[]):
    for fl in fls:
        rn = raw_name(fl)
        with open(fl) as f:
            if not rn in tab:
                tab[rn]=[]
            l = to_list(f.readlines()[0],delim=args.d,escape=args.escape)
            lnew = []
            for i, e in enumerate(l):
                if not i in exclude:
                    lnew.append(e)
            tab[rn] += lnew

def highlight_min(tab,x,y,highlight='\\textbf{%s}'):
    for k,l in tab.items():
        a = l[x]
        b = l[y]
        av = float(a.replace('\\','').replace('%',''))
        bv = float(b.replace('\\','').replace('%',''))
        if av < bv:
            l[x] = highlight%a
        elif bv < av:
            l[y] = highlight%b
        

tab = {}


add_to_tab(args.files,tab,exclude=args.f1exclude)
add_to_tab(args.files2,tab,exclude=args.f2exclude)
add_to_tab(args.files3,tab,exclude=args.f3exclude)        
num_cols = 1
num_cols += max([len(v) for v in tab.values()])
keyz = sorted(list(tab.keys()))

if args.highlight_min:
    highlight_min(tab,args.highlight_min[0],args.highlight_min[1])

print("\\begin{tabular}{"+(num_cols*"c")+"}")
if args.header:
    print(' & '.join(args.header),end="\\\\\n")
for k in keyz:
    print(' & '.join(([name_to_latex(k)]+tab[k])),end="\\\\\n")
    
print("\\end{tabular}")

